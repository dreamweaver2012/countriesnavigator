#  CountriesNavigator
@author Dragos-Costin Manea


Pre-requisites:
Run the project on a machine having external acces to the Internet, in order to be able to access the countries data provided at the following address:
https://raw.githubusercontent.com/mledoze/countries/master/countries.json

The project uses Java17+ (required for the dependencies) and Maven.
For additional infos on how to install these tools follow the instructions from the following resources:
Java: https://docs.oracle.com/
Maven: https://maven.apache.org/install.html

After you have installed them you can
1. Build the project by running the following command in the console, opened in the project's root folder:
mvn clean install
In the "target" folder you should have an artifact called "navigator-0.0.1-SNAPSHOT.jar".

2. Run the project.
After building it, you can navigate to the subfolder called "target" and run it manually by issuing the following command in the console:
java -jar navigator-0.0.1-SNAPSHOT.jar

3. Run the tests with the following command in the root folder: 
mvn test

4. After running the project, you can use the service by accesing the REST end-point
/routing/{origin}/{destination}

Example:
If the application is started on the localhost, on port 8080, use something like:
http://localhost:8080/routing/CAN/MEX


Alternatively, you can import the project in your favorite IDE (it was developed in Eclipse), using the option to import as a Maven Project.
Then, you can create a Run config where to supply the appropiate JDK or you can use the IDE's terminal to proceed as above.

Project components (classes):
A. Implementation 
- NavigatorApplication.java - Launches the application.
- CountriesNavigatorService - Implements the path detection logic. 
- NavigatorController - Exposes the REST end-point
		
B. Test classes
- NavigatorControllerUnitTest - Unit tests for the Rest Controller
- NavigatorControllerIntegrationTest - Tests the Rest Controller by providing a Spring Context.

Additional notes:
I haven't implemented the Dijkstra algorithm from scratch, because I have studied it during high-school classes, back in 1997, and it's something standard. 
I think every developer should know about it, but I don't see any reason to re-implement it in my projects. That's why I have used JGraphT library instead, https://jgrapht.org/, which I have used in a previous commercial project, developed by one of my employers.