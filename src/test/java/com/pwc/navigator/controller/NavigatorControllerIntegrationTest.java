package com.pwc.navigator.controller;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.pwc.navigator.NavigatorApplication;
import com.pwc.navigator.service.CountriesNavigatorService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = NavigatorApplication.class)
@AutoConfigureMockMvc
public class NavigatorControllerIntegrationTest {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private CountriesNavigatorService countriesNavigatorService;

	@Test
	public void givenInexisting_country_thenStatus400() throws Exception {
		mvc.perform(get("/routing/XYZ/ITA").contentType("application/json")).andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
	}

	@Test
	public void givenCountries_on_different_continents_thenStatus400() throws Exception {
		mvc.perform(get("/routing/ROU/USA").contentType("application/json")).andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
	}

	@Test
	public void givenCountries_on_the_same_continent_ROU_ITA_thenStatus200_and_path() throws Exception {
		mvc.perform(get("/routing/ROU/ITA").contentType("application/json")).andExpect(status().is(HttpStatus.OK.value())).andExpect(
				content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)).andExpect(
						jsonPath("$.route", is(Arrays.asList(new String[] { "ROU", "HUN", "AUT", "ITA" }))));
	}

	@Test
	public void givenCountries_on_the_same_continent_CZE_ITA_thenStatus200_and_path() throws Exception {
		mvc.perform(get("/routing/CZE/ITA").contentType("application/json")).andExpect(status().is(HttpStatus.OK.value())).andExpect(
				content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)).andExpect(jsonPath("$.route", is(Arrays.asList(new String[] { "CZE", "AUT", "ITA" }))));
	}
}
