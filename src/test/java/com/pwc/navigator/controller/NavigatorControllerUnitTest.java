package com.pwc.navigator.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.pwc.navigator.json.NavigatorRouteResponse;
import com.pwc.navigator.service.CountriesNavigatorService;

@ExtendWith(MockitoExtension.class)
public class NavigatorControllerUnitTest {

	@InjectMocks
	NavigatorController navigatorController;

	@Mock
	private CountriesNavigatorService countriesNavigatorService;

	@Test
	public void test_CZE_to_ITA() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

		NavigatorRouteResponse nr = new NavigatorRouteResponse();
		nr.setRoute(Arrays.asList(new String[] { "CZE", "AUT", "ITA" }));

		when(countriesNavigatorService.getRouteBetweenCountries("CZE", "ITA")).thenReturn(nr);

		ResponseEntity<NavigatorRouteResponse> responseEntity = navigatorController.getRoute("CZE", "ITA");
		assertThat(responseEntity.getStatusCode().value()).isEqualTo(200);
		assertThat(responseEntity.getBody()).isNotNull();
		assertThat(responseEntity.getBody().getRoute()).isNotNull();
		assertThat(responseEntity.getBody().getRoute()).isEqualTo(Arrays.asList(new String[] { "CZE", "AUT", "ITA" }));
	}

	@Test
	public void test_ROU_to_USA() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

		NavigatorRouteResponse nr = new NavigatorRouteResponse();

		when(countriesNavigatorService.getRouteBetweenCountries("ROU", "USA")).thenReturn(nr);

		ResponseEntity<NavigatorRouteResponse> responseEntity = navigatorController.getRoute("ROU", "USA");
		assertThat(responseEntity.getStatusCode().value()).isEqualTo(400);
		assertThat(responseEntity.getBody()).isNull();
	}
}
