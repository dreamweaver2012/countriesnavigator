package com.pwc.navigator.json;

import java.util.List;

/**
 * 
 * @author Dragos Manea
 *
 * Class for mapping the data read from the countries data.
 */
public class Country {

	private Object name;
	private List<String> tld;
	private String cca2;
	private String ccn3;
	private String cca3;

	private String cioc;
	private Boolean independent;
	private String status;
	private Boolean unMember;
	private Object currencies;

	private Object idd;
	private List<String> capital;
	private List<String> altSpellings;
	private String region;
	private String subregion;

	private Object languages;
	private Object translations;

	private List<Double> latlng;

	private Boolean landlocked;
	private Double area;
	private String flag;

	private Object demonyms;

	private List<String> borders;

	public String getCca3() {
		return cca3;
	}

	public void setCca3(String cca3) {
		this.cca3 = cca3;
	}

	public List<String> getBorders() {
		return borders;
	}

	public void setBorders(List<String> borders) {
		this.borders = borders;
	}

	public List<String> getTld() {
		return tld;
	}

	public void setTld(List<String> tld) {
		this.tld = tld;
	}

	public String getCca2() {
		return cca2;
	}

	public void setCca2(String cca2) {
		this.cca2 = cca2;
	}

	public String getCcn3() {
		return ccn3;
	}

	public void setCcn3(String ccn3) {
		this.ccn3 = ccn3;
	}

	public String getCioc() {
		return cioc;
	}

	public void setCioc(String cioc) {
		this.cioc = cioc;
	}

	public Boolean getIndependent() {
		return independent;
	}

	public void setIndependent(Boolean independent) {
		this.independent = independent;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getUnMember() {
		return unMember;
	}

	public void setUnMember(Boolean unMember) {
		this.unMember = unMember;
	}

	public Object getCurrencies() {
		return currencies;
	}

	public void setCurrencies(Object currencies) {
		this.currencies = currencies;
	}

	public Object getIdd() {
		return idd;
	}

	public void setIdd(Object idd) {
		this.idd = idd;
	}

	public List<String> getCapital() {
		return capital;
	}

	public void setCapital(List<String> capital) {
		this.capital = capital;
	}

	public List<String> getAltSpellings() {
		return altSpellings;
	}

	public void setAltSpellings(List<String> altSpellings) {
		this.altSpellings = altSpellings;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getSubregion() {
		return subregion;
	}

	public void setSubregion(String subregion) {
		this.subregion = subregion;
	}

	public Object getLanguages() {
		return languages;
	}

	public void setLanguages(Object languages) {
		this.languages = languages;
	}

	public Object getTranslations() {
		return translations;
	}

	public void setTranslations(Object translations) {
		this.translations = translations;
	}

	public List<Double> getLatlng() {
		return latlng;
	}

	public void setLatlng(List<Double> latlng) {
		this.latlng = latlng;
	}

	public Boolean getLandlocked() {
		return landlocked;
	}

	public void setLandlocked(Boolean landlocked) {
		this.landlocked = landlocked;
	}

	public Double getArea() {
		return area;
	}

	public void setArea(Double area) {
		this.area = area;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public Object getDemonyms() {
		return demonyms;
	}

	public void setDemonyms(Object demonyms) {
		this.demonyms = demonyms;
	}

	public Object getName() {
		return name;
	}

	public void setName(Object name) {
		this.name = name;
	}

}
