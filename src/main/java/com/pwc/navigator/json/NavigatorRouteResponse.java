package com.pwc.navigator.json;

import java.util.List;

/**
 * 
 * @author Dragos Manea
 * Class mapping the required result in the desired response format.
 */
public class NavigatorRouteResponse {
	private List<String> route;

	public List<String> getRoute() {
		return route;
	}

	public void setRoute(List<String> path) {
		this.route = path;
	}

}
