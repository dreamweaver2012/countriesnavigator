package com.pwc.navigator.dto;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultWeightedEdgeWrapper {
	private DefaultWeightedEdge edge;
	
	private static final Logger logger = LoggerFactory.getLogger(DefaultWeightedEdgeWrapper.class);

	public DefaultWeightedEdgeWrapper(DefaultWeightedEdge e) {
		this.edge = e;
	}

	public String getSource() {
		try {
			Method getSource = DefaultWeightedEdge.class.getDeclaredMethod("getSource");
			getSource.setAccessible(true);
			return (String) getSource.invoke(edge);
		}
		catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
			logger.error("Could not acces graph's edge data: " + ex.getMessage());
			return null;
		}
	}

	public String getTarget() {
		try {
			Method getTarget = DefaultWeightedEdge.class.getDeclaredMethod("getTarget");
			getTarget.setAccessible(true);
			return (String) getTarget.invoke(edge);
		}
		catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
			logger.error("Could not acces graph's edge data: " + ex.getMessage());
			return null;
		}
	}

}
