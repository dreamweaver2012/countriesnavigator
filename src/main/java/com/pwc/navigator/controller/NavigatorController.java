package com.pwc.navigator.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pwc.navigator.json.NavigatorRouteResponse;
import com.pwc.navigator.service.CountriesNavigatorService;


/**
 * 
 * @author Dragos Manea
 *
 */

@RestController
@RequestMapping("/")
public class NavigatorController {

	@Autowired
	private CountriesNavigatorService countriesNavigatorService;

	@GetMapping("/routing/{origin}/{destination}")
	public ResponseEntity<NavigatorRouteResponse> getRoute(@PathVariable(value = "origin")
	String origin, @PathVariable(value = "destination")
	String destination) {
		NavigatorRouteResponse route = countriesNavigatorService.getRouteBetweenCountries(origin, destination);
		if (route.getRoute() == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
		return ResponseEntity.ok().body(route);
	}
}
