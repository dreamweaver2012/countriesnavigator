package com.pwc.navigator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author Dragos Manea
 *
 */

@SpringBootApplication
public class NavigatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(NavigatorApplication.class, args);

	}

}
