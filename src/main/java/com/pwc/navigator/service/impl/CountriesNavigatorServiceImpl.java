package com.pwc.navigator.service.impl;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.builder.GraphTypeBuilder;
import org.jgrapht.util.SupplierUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pwc.navigator.dto.DefaultWeightedEdgeWrapper;
import com.pwc.navigator.json.Country;
import com.pwc.navigator.json.NavigatorRouteResponse;
import com.pwc.navigator.service.CountriesNavigatorService;

import jakarta.annotation.PostConstruct;

/**
 * 
 * @author Dragos Manea
 *
 */

@Service
public class CountriesNavigatorServiceImpl implements CountriesNavigatorService {

	private Map<Integer, String> vertexesByCountryIndexMap = new HashMap<Integer, String>();
	private Map<String, Integer> countriesIndexByName = new HashMap<String, Integer>();
	private Map<String, String> countriesNameByIndex = new HashMap<String, String>();
	private DijkstraShortestPath<String, DefaultWeightedEdge> dijkstraDetector;

	@Value("${countries_map.url}")
	private String countriesMapUrl;

	private final Logger logger = LoggerFactory.getLogger(CountriesNavigatorServiceImpl.class);

	/**
	 * Calculates a land route between 2 countries specified as arguments.
	 * @param start - Starting country of the land route 
	 * @param destination - End country of the land route
	 * @return - An object that encapsulates a list of string, representing the names of the countries of the required route.
	 */
	@Override
	public NavigatorRouteResponse getRouteBetweenCountries(String start, String destination) {
		NavigatorRouteResponse navigatorRoute = new NavigatorRouteResponse();

		String startCountry = vertexesByCountryIndexMap.get(countriesIndexByName.get(start));
		String destinationCountry = vertexesByCountryIndexMap.get(countriesIndexByName.get(destination));
		if (startCountry == null || destinationCountry == null) {
			return navigatorRoute;
		}

		GraphPath<String, DefaultWeightedEdge> graphPath = dijkstraDetector.getPath(startCountry, destinationCountry);
		if (graphPath == null) {
			return navigatorRoute;
		}
		String currentCountry = startCountry;
		List<String> path = new ArrayList<String>();
		path.add(countriesNameByIndex.get(currentCountry));
		for (DefaultWeightedEdge dwe : graphPath.getEdgeList()) {
			DefaultWeightedEdgeWrapper edgeWrapper = new DefaultWeightedEdgeWrapper(dwe);

			if (edgeWrapper.getSource().equals(currentCountry)) {
				path.add(countriesNameByIndex.get(edgeWrapper.getTarget()));
				currentCountry = edgeWrapper.getTarget();
			}
			else {
				path.add(countriesNameByIndex.get(edgeWrapper.getSource()));
				currentCountry = edgeWrapper.getSource();
			}
		}

		navigatorRoute.setRoute(path);
		return navigatorRoute;
	}

	/**
	 * Builds an undirected graph based on the countries data. 
	 * For each country found in the provided JSON data an vertex is created.
	 * If two countries have a common border an edge between the corresponding vertexes is created.
	 * Based on the previously built graph a minimum path detector is built, based on the Dijkstra algorithm.
	 */
	@PostConstruct
	private void initComputationData() {
		Country[] countries = initCountriesData();
		Graph<String, DefaultWeightedEdge> graph = GraphTypeBuilder.undirected().weighted(false).allowingMultipleEdges(false).allowingSelfLoops(false).vertexSupplier(
				SupplierUtil.createStringSupplier()).edgeSupplier(SupplierUtil.createDefaultWeightedEdgeSupplier()).buildGraph();

		if (countries != null) {
			for (int i = 0; i < countries.length; i++) {
				vertexesByCountryIndexMap.put(i, graph.addVertex());
				countriesIndexByName.put(countries[i].getCca3(), i);
				countriesNameByIndex.put(String.valueOf(i), countries[i].getCca3());
			}
			for (int i = 0; i < countries.length; i++) {
				if (countries[i].getBorders() != null) {
					for (int j = 0; j < countries[i].getBorders().size(); j++) {
						graph.addEdge(vertexesByCountryIndexMap.get(i), vertexesByCountryIndexMap.get(countriesIndexByName.get(countries[i].getBorders().get(j))));
					}
				}
			}
		}
		dijkstraDetector = new DijkstraShortestPath<String, DefaultWeightedEdge>(graph);
	}

	/**
	 * Read countries configuration data
	 * @return
	 */
	private Country[] initCountriesData() {
		ObjectMapper mapper = new ObjectMapper();

		try {
			return mapper.readValue(new URL(countriesMapUrl), Country[].class);
		}
		catch (IOException ioe) {
			logger.error("Could not read countries map data: " + ioe.getMessage());
			return null;
		}
	}

}
