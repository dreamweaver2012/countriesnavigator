package com.pwc.navigator.service;

import com.pwc.navigator.json.NavigatorRouteResponse;

public interface CountriesNavigatorService {

	NavigatorRouteResponse getRouteBetweenCountries(String start, String destination);
}
